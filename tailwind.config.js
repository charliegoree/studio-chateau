/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Source Sans Pro", 'Helvetica', 'sans-serif'],
        serif: ["Merriweather", 'serif']
      },
      colors: {
        'white-transparent': '#ffffff66'
      }
    },
  },
  plugins: [],
}