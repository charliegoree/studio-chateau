import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Page() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Biblioteca Municipal'
            />

            <div className='w-10/12'>

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/biblioteca/Portada.png'
            />

            </div>

            <BackToProject/>
        </section>
    );
}
