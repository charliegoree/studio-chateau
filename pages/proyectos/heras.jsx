import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Heras() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                    <div>
                        Vivienda unifamiliar ubicada en la localidad de Las Heras, provincia de Mendoza.<br/> 
                        Nuevamente en este proyecto mostramos una de nuestras obras arquitectónicas realizadas en el país, por lo que fue proyectada desde cero por nuestro estudio a pedido de nuestros clientes, que impusieron en el diseño las 4 torres recorribles de la vivienda.<br/> Consta con un trabajo de diseño y proyección íntegro, yendo de la cimentación hasta el techado.
                    </div>
                )}
                title='Castillo Las Heras'
            />

            <div className='w-10/12'>

            <ImgMel
                width='5/12'
                altt='castillo las heras'
                src='/images/heras/Portada.png'
            />
            <p className='italic'>Como elementos arquitectónicos del Renacimiento Francés utilizados se destacan:</p>

            <ul className='italic'>
                <li>
                    <p><span className='font-black'>•</span> La simetría</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Las chimeneas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Techos inclinados con formas cónicas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Ventanas con cornizas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> 3 niveles</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Repetición de ventanas</p>
                </li>
            </ul>


            <ImgMel
                width='5/12'
                altt='castillo las heras'
                src='/images/heras/Frame 20.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las heras'
                src='/images/heras/Frame 21.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las heras'
                src='/images/heras/Frame 22.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las heras'
                src='/images/heras/Frame 23.png'
            />

            </div>

            <BackToProject/>
        </section>
    );
}
