import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Sierras() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                    <div>
                        Hotel cinco estrellas ubicado en la localidad de Tandil, provincia de Buenos Aires. <br/>
                        Es el proyecto más grande realizado hasta el momento, cuenta con una superficie de 116 646 m² construidos, repartidos en 1500 hectáreas.<br/>                                                                                                        Este proyecto fue plasmado en base al casco antiguo del hotel, y nuestro diseño fue acoplándose a este. Trabajo de proyección íntegramente realizado por nuestro estudio, que cuenta con intervenciones de paisajismo en los jardines como en sus interiores, convirtiéndose así  
                        en un glamuroso castillo ubicado en el interior de la provincia.
                    </div>
                )}
                title='Hotel castillo de las sierras'
            />

            <div className='w-10/12'>

            <ImgMel
                width='5/12'
                altt='hotel castillo las sierras'
                src='/images/sierras/Portada.png'
            />
            <p className='italic'>Como elementos arquitectónicos del Renacimiento Francés utilizados se destacan:</p>

            <ul className='italic'>
                <li>
                    <p><span className='font-black'>•</span> La simetría</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Las chimeneas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Arcos de 1/2 punto</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Techos inclinados</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Ventanas con cornizas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Terrazas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Jardín</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Repetición de ventanas</p>
                </li>
            </ul>


            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 4.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 5.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 6.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 7.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 9.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 10.png'
            />

            <ImgMel
                width='5/12'
                altt='castillo las sierras'
                src='/images/sierras/Frame 11.png'
            />

            </div>

            <BackToProject/>
        </section>
    );
}
