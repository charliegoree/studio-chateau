import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Biblioteca() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                <div>
                    Biblioteca Municipal de Olavarría, provincia de Buenos Aires.<br/>
                Por motivo de su aniversario fue reformada y transformada, a pedido y gusto de la Municipalidad de Olavarría, al estilo renacentista francés, dándole así características principales del estilo en todo su exterior.<br/>
                Este trabajo constó con intervenciones de mampostería, estructura, terminaciones exteriores, interiores, carpinterías, techado y paisajismo.
                </div>
                )}
                title='Biblioteca Municipal'
            />
            <div className='w-10/12'>

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Portada.png'
                />

                <p className='italic'>Como elementos arquitectónicos del Renacimiento Francés utilizados se destacan:</p>

                <ul className='italic'>
                    <li>
                        <p><span className='font-black'>•</span> La simetría</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Las chimeneas</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Arcos de 1/2 punto</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Techo inclinado</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Ventanas con cornisas</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Jardín</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Piedra caliza</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> 3 niveles</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Repetición de ventanas</p>
                    </li>
                </ul>
            
                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 25.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 26.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 27.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 28.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 29.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/biblioteca/Frame 30.png'
                />
            </div>

            <BackToProject/>
        </section>
    );
}
