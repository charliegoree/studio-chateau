import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Pilar() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                    <div>
                        Vivienda unifamiliar ubicada en la localidad de Pilar, provincia de Buenos Aires.<br/>
                        Este proyecto es el más reciente de los concluidos, tiene la peculiaridad de haber sido proyectado dentro de un barrio cerrado, lo cual nos generó algunos impedimentos a la hora de construir debido a las normas que allí rigen.<br/>
                        No obstante, pudimos desenvolver nuestras ideas y plasmarlas de forma apropiada, quedando así la casa de ensueño de nuestros clientes.
                    </div>
                )}
                title='Vivienda Country Pilar'
            />

            <div className='w-10/12'>

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/pilar/Portada.png'
            />

            <p className='italic'>Como elementos arquitectónicos del Renacimiento Francés utilizados se destacan:</p>

            <ul className='italic'>
                <li>
                    <p><span className='font-black'>•</span> La simetría</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Las chimeneas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Techos inclinados con formas cónicas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Ventanas con cornisas</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Jardín</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Piedra caliza</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> 3 niveles</p>
                </li>
                <li>
                    <p><span className='font-black'>•</span> Repetición de ventanas</p>
                </li>
            </ul>
            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/pilar/Frame 2.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/pilar/Frame 3.png'
            />

            </div>

            <BackToProject/>
        </section>
    );
}
