import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Interiores() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                    <div>
                        Con nuestro estudio no solo vas a poder tener una fachada inspirada en el renacimiento, también lo llevamos al interior y todo lo que esto concierne,  desde las grandes chimeneas hasta los techos abovedados y sus terminaciones.<br/>
                        Trabajando también con línea de muebles originales certificados  conseguidos por nosotros mismos.<br/>
                        Creando así un ambiente acogedor e ideal para tu vivienda.
                    </div>
                )}
                title='Interiores'
            />

            <div className='w-10/12'>

            <ImgMel
                width='5/12'
                altt='interiores'
                src='/images/interiores/Portada.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 31.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 32.png'
            />
            <ImgMel
                width='5/12'
                altt='interiores'
                src='/images/interiores/Frame 33.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 34.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 35.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 37.png'
            />
            <ImgMel
                width='5/12'
                altt='interiores'
                src='/images/interiores/Frame 39.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 40.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 41.png'
            />
            <ImgMel
                width='5/12'
                altt='interiores'
                src='/images/interiores/Frame 42.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 43.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 44.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 45.png'
            />
            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 46.png'
            />
            <ImgMel
                width='5/12'
                altt='interiores'
                src='/images/interiores/Frame 47.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 48.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 49.png'
            />

            <ImgMel
                width='5/12'
                altt='biblioteca'
                src='/images/interiores/Frame 50.png'
            />

            </div>

            <BackToProject/>
        </section>
    );
}