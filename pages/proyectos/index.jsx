import Title from '../../components/title.jsx';
import List from '../../components/list.jsx';
import ImgMel from '../../components/imgMel.jsx';

export default function Proyectos() {
    const lista = [
        {title: 'Biblioteca Municipal', message: `Biblioteca Municipal de Olavarría, provincia de Buenos Aires.
        Por motivo de su aniversario fue reformada y transformada, a pedido y gusto de la Municipalidad de Olavarría, al estilo renacentista francés, dándole así características principales del estilo en todo su exterior.
        Este trabajo constó con intervenciones de mampostería, estructura, terminaciones exteriores, interiores, carpinterías, techado y paisajismo.`, url: '/proyectos/biblioteca', image: '/images/biblioteca/Portada.png'},
        {title: 'Estancia Rousillon', message: `Estancia en las afueras de Lobería, provincia de Buenos Aires.
        El cliente buscaba que su vivienda luzca este estilo debido a que sus familiares fueron inmigrantes franceses. La construcción, diseño, proyección y paisajismo de la vivienda unifamiliar fue íntegramente realizada por nuestro estudio y personal, pasando así, por todas las etapas de la obra hasta su eventual construcción.`, url: '/proyectos/rousillon', image: '/images/rousillon/portada.png'},
    ];
    const lista2 = [
        {title: 'Castillo Las Heras', message: `Vivienda unifamiliar ubicada en la localidad de Las Heras, provincia de Mendoza.  
        Nuevamente en este proyecto mostramos una de nuestras obras arquitectónicas realizadas en el país, por lo que fue proyectada desde cero por nuestro estudio a pedido de nuestros clientes, que impusieron en el diseño las 4 torres recorribles de la vivienda. Consta con un trabajo de diseño y proyección íntegro, yendo de la cimentación hasta el techado.`, url: '/proyectos/heras', image: '/images/heras/portada.png'},
        {title: 'Vivienda Country Pilar', message: `Vivienda unifamiliar ubicada en la localidad de Pilar, provincia de Buenos Aires.
        Este proyecto es el más reciente de los concluidos , tiene la peculiaridad de haber sido proyectado dentro de un barrio cerrado, lo cual nos generó algunos impedimentos a la hora de construir debido a las normas que allí rigen. No obstante, pudimos desenvolver nuestras ideas y plasmarlas de forma apropiada, quedando así la casa de ensueño de nuestros clientes.`, url: '/proyectos/pilar', image: '/images/pilar/portada.png'}
    ];

    const lista3 = [
        {title: 'Hotel castillo de las sierras', message: `Hotel cinco estrellas ubicado en la localidad de Tandil, provincia de Buenos Aires. 
        Es el proyecto más grande realizado hasta el momento, cuenta con una superficie de 116 646 m² construidos, repartidos en 1500 hectáreas.
        Este proyecto fue plasmado en base al casco antiguo del hotel, y nuestro diseño fue acoplándose a este. Trabajo de proyección íntegramente realizado por nuestro estudio, que cuenta con intervenciones de paisajismo en los jardines como en sus interiores, convirtiéndose así  
        en un glamuroso castillo ubicado en el interior de la provincia.`, url: '/proyectos/sierras', image: '/images/sierras/portada.png'},
        {title: 'Interiores', message: `Con nuestro estudio no solo vas a poder tener una fachada inspirada en el renacimiento, también lo llevamos al interior y todo lo que esto concierne,  desde las grandes chimeneas hasta los techos abovedados y sus terminaciones. Trabajando también con línea de muebles originales certificados  conseguidos por nosotros mismos. Creando así un ambiente acogedor e ideal para tu vivienda.`, url: '/proyectos/interiores', image: '/images/interiores/portada.png'}
    ];

    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Nuestros proyectos'
            />

            <List list={lista}/>

            <List list={lista2}/>

            <List list={lista3}/>
        </section>
    );
}