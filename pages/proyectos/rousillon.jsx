import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToProject from '../../components/BackToProject.jsx';

export default function Rousillon() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                subtitle={(
                    <div>
                        Estancia en las afueras de Lobería, provincia de Buenos Aires.<br/>
                        El cliente buscaba que su vivienda luzca este estilo debido a que sus familiares fueron inmigrantes franceses.<br/>
                        La construcción, diseño, proyección y paisajismo de la vivienda unifamiliar fue íntegramente realizada por nuestro estudio y personal, pasando así, por todas las etapas de la obra hasta su eventual construcción.
                    </div>
                )}
                title='Estancia Rousillon'
            />

            <div className='w-10/12'>
                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Portada.png'
                />

                <p className='italic'>Como elementos arquitectónicos del Renacimiento Francés utilizados se destacan:</p>

                <ul className='italic'>
                    <li>
                        <p><span className='font-black'>•</span> La simetría</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Las chimeneas</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Arcos de 1/2 punto</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Techo inclinado</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Jardín</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Piedra caliza</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> 3 niveles</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Repetición de ventanas</p>
                    </li>
                </ul>

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 12.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 14.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 15.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 16.png'
                />

                <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 17.png'
                />

                 <ImgMel
                    width='5/12'
                    altt='biblioteca'
                    src='/images/rousillon/Frame 18.png'
                />
            </div>

            <BackToProject/>
        </section>
    );
}
