import '../styles/globals.css';
import 'remixicon/fonts/remixicon.css';
import { useRouter } from 'next/router';
import { useState, useEffect, useCallback, useRef } from 'react';
import Link from 'next/link';

const menuItems = [
  {url: '/', text: 'STUDIO Château'},
  {url: '/historia', text: 'HISTORIA'},
  {url: '/proyectos', text: 'PROYECTOS'}
];

function MyApp({ Component, pageProps }) {
  const { pathname } = useRouter();
  const videoPlayer = useRef(null);

  const [withTitle, setWithTitle] = useState(false);
  const [reduce, setReduce] = useState(false);

  function toBack() {
    document.documentElement.scrollTop = 300;
  }

  function renderTitle() {
    return (
      <div className={`absolute t-0 l-0 w-full duration-1000 ease-in-out flex flex-col items-center justify-center font-bold text-white`}>
          <div
            style={{top: '6rem'}}
            className={`absolute duration-1000 ease-in-out flex flex-col items-center justify-center ${!withTitle ? 'opacity-0 translate-y' : 'opacity-100'}`}
          >
          
            <h1 className='uppercase leading-10 text-7xl'>Studio</h1>
            <h1 className='uppercase leading-10 text-7xl'>Château</h1>

            <p className='font-light text-xl italic mt-6 text-center'>Fundamentalistas del Renacimiento Francés</p>

            <i onClick={toBack} className='ri-arrow-down-circle-line text-5xl font-medium p-4 cursor-pointer'/>
          </div>
        

        <h1 style={{top: '6rem'}} className={`absolute duration-1000 ease-in-out text-3xl uppercase border-solid border-white border-4 p-4 ${withTitle ? 'opacity-0' : 'opacity-100'}`}>
          Studio Château
        </h1>
      </div>
    );
  }

  function renderFooter() {
    return (
      <div className='z-2 relative p-2 md:p-4 w-full flex flex-col items-center justify-center text-white-transparent text-center'>
        <div className='flex justify-between align-center divide-x-2  divide-white-transparent'>
          <h4 className='sm:text-xs text-sm px-4 md:px-8'>Instituto Palladio</h4>

          <h4 className='text-xs md:text-sm px-2 md:px-8'>1er año interiorismo</h4>

          <h4 className='text-xs md:text-sm px-2 md:px-8'>TP: Renacimiento</h4>
        </div>

        <div className='flex justify-between align-center divide-x-2  divide-white-transparent'>
          <h4 className='text-xs md:text-sm px-2 md:px-8'>Profesora: Alicia Velazco</h4>

          <h4 className='text-xs md:text-sm px-2 md:px-8'>Alumnos: Valentino Paiz & Melina Di Modica</h4>
        </div>
      </div>
    );
  }

  const handleScroll = useCallback(e => {
    if (document.documentElement.scrollTop > 300) {
      setReduce(true);
    } else {
      setReduce(false);
    }
  
    if (pathname === '/' && document.documentElement.scrollTop > 200 && withTitle) {
      document.documentElement.scrollTop = 0;
      setWithTitle(false);
    }
  }, [pathname, withTitle]);

  useEffect(() => {
    document.addEventListener('scroll', handleScroll);
    handleScroll();

    return () => document.removeEventListener('scroll', handleScroll);
  }, [handleScroll]);

  useEffect(() => {
    if (pathname) {
      setWithTitle(pathname === '/');
    }
  }, [pathname]);

  useEffect(() => {
    if (videoPlayer.current) {
      videoPlayer.current.play();
    }
  }, [videoPlayer.current]);

  return (
    <main
    style={{overflowX: 'hidden'}}
    className='w-full min-h-screen pb-8 bg-black-500 text-slate-800 bg-fixed bg-center bg-cover flex flex-col items-center justify-start'>
      <div style={{
        width: '100%',
        height: '100vh',
        overflow: 'hidden',
        position: 'fixed',
        left: '0',
        top: '0'
      }}>
        <video ref={videoPlayer} autoPlay muted loop style={{
          objectFit: 'cover',
          width: '100%',
          height: '100%'
        }}>
          <source src='/background.mp4' type='video/mp4'/>
        </video>
      </div>

      {renderTitle()}

      <div className='duration-1000 z-2 relative container flex align-center justify-center md:justify-start bg-white-transparent' style={{ marginTop: reduce || !withTitle ? '16rem' : '80vh'}}>
        {menuItems.map(item => (
          <Link
            href={item.url}
            key={item.text}
          >
            <h4 className={`h-full text-xs md:text-sm p-6 md:p-5 m-0 ${item.url === pathname ? 'bg-white text-slate-800' : 'text-white'} hover:bg-white-transparent uppercase font-bold hover:text-white hover:cursor-pointer ease-in duration-200`}>
              {item.text}
            </h4>
          </Link>
        ))}
      </div>

      <div className='container bg-white py-8 sm:px-0 px-4 flex flex-col items-center justify-start drop-shadow-md'>
      <Component {...pageProps} />
      </div>
      
      {renderFooter()}
    </main>
  );
}

export default MyApp
