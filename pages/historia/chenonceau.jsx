import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToHistory from '../../components/BackToHistory.jsx';

export default function Chenonceau() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Castillo de Chenonceau'
                subtitle='Construído en el año 1513 por Katherine Briconnet, luego decorado por Diana de Poitiers y Catalina de Médicis.'
            />
            <div className='w-10/12'>
                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-1.jpeg'
                />
            
                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-2.jpeg'
                />

                <h2 className='font-bold text-3xl text-center mb-12'>
                    Elementos arquitectónicos utilizados
                </h2>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-3.jpeg'
                />

                <h2 className='font-bold text-3xl text-center mb-12'>
                    Espacios
                </h2>

                <p className='text-center italic font-bold'>Galería: Presenta suelo ajedrezado de toba y pizarra, en el techo se distinguen las vigas y presenta 2 chimeneas renacentistas.</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-4.jpeg'
                />

                <p className='text-center italic font-bold'>Vestíbulo: Cubierto por bóvedas de ojiva, cuyas claves forman una línea quebrada. Las ménsulas están adornadas con follajes, rosas, cabezas de ángeles, quimeras y cuernos de la abundancia.</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-5.jpeg'
                />

                <p className='text-center italic font-bold'>Escalera: De doble rampa, cubierta de una bóveda con casetones.</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-6.jpeg'
                />

                <p className='text-center italic font-bold'>Jardines</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-7.jpeg'
                />

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-8.jpeg'
                />

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chenonceau/image-9.jpeg'
                />
                
                <h2 className='font-bold text-3xl text-center mb-12'>
                    Conozcamoslo por dentro
                </h2>

                <iframe src="https://www.google.com/maps/embed?pb=!4v1664173218861!6m8!1m7!1sy4Z8qgSddbFyByrSyL5nfw!2m2!1d47.32436401666595!2d1.070635814680912!3f0!4f0!5f0.4000000000000002" width="600" height="600" className='w-11/12 m-auto' style={{border:0 }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>

            <BackToHistory/>
        </section>
    );
}