import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToHistory from '../../components/BackToHistory.jsx';

export default function Mobilario() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Mobilario'
                subtitle={(<div>El Renacimiento Francés se divide en tres períodos:
                    Francisco I (1515-1547): Se inicia el Renacimiento en Francia y luego se llega a su apogeo.<br/>
                    Enrique II (1547-1559): Se dividen en dos escuelas: La del Noroeste, donde utilizaban el roble y ornamentaciones sobrias, con aun permanencia del Gótico, y del Sureste, donde utilizaban el nogal blando, por eso eran muy tallados.<br/>
                    Luis XIII (1610-1643): Transición hacia el estilo Barroco.</div>)}
            />

            <div className='w-10/12'>
            <p className='italic'>Características principales del mueble renacentista Francés:</p>

            <ul className='italic'>
                <li><p><span className='font-black'>•</span> Permanencia del estilo italiano</p></li>
                <li><p><span className='font-black'>•</span> Son tratados como edificios, con sus respectivos elementos arquitectónicos</p></li>
                <li><p><span className='font-black'>•</span> Se dividen en secciones o paneles por columnas, pilastras, cariátides o bandas molduradas</p></li>
                <li><p><span className='font-black'>•</span> Son de roble, nogal y al final de la época se introduce el ébano</p></li>
                <li><p><span className='font-black'>•</span> Utilizan motivos clásicos para la decoración</p></li>
                <li><p><span className='font-black'>•</span> Al final de la época se introduce el bronce y la marquetería</p></li>
            </ul>

            <p className='text-center italic font-bold'>Armario de lujo del siglo XVI,  tallado de dos cuerpos, seccionado en dos paneles por cariátides. Posee un zócalo en la parte inferior y una cornisa en la superior.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-2.jpeg'
            />

            <p className='text-center italic font-bold'>Armario de dos cuerpos del siglo XVI, el cuerpo superior es más pequeño que el inferior. Se puede destacar su parecido con una construcción arquitectónica.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-3.jpeg'
            />

            <p className='text-center italic font-bold'>Arca del siglo XVI con un amplio zócalo decorado con tallas de vegetación simétricas que derivan en quimeras. También se secciona por pilastras y se destaca la cara de perfil y la concha como motivos muy característicos de este estilo.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-4.jpeg'
            />

            <p className='text-center italic font-bold'>Sillón de la segunda mitad del XVI. Posee en la parte superior, un respaldo con estructura de arcos de medio punto, y un frontón, en la inferior, tres columnas torneadas romanas.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-5.jpeg'
            />

            <p className='text-center italic font-bold'>Mesa de la segunda mitad del XVI de tablero grueso, formando como una cornisa, apoyado sobre cuatro pilastras compuestas, unidas por un arco con dos quimeras a sus lados, y sobre cuatro arcos de medio punto, unidos por balaustres.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-6.jpeg'
            />

            <p className='text-center italic font-bold'>Mesa de la segunda mitad del XVI, de diseño sobrio y elegante, con tablero extensible apoyado sobre nueve columnas, que están sostenidas por una especie de friso.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-7.jpeg'
            />

            <p className='text-center italic font-bold'>Cama con dosel de fines del Renacimiento, con cornisa y friso apoyados sobre balaustres tallados y su cabecero sumamente tallado con ornamentación de motivos vegetales.</p>

            <ImgMel
                width='5/12'
                altt='plano del castillo'
                src='/images/mobilario/image-1.jpeg'
            />
            </div>
            
            <BackToHistory/>
        </section>
    );
}