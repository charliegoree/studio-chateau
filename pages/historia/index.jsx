import Title from '../../components/title.jsx';
import List from '../../components/list.jsx';
import ImgMel from '../../components/imgMel.jsx';

export default function Historia() {
    const lista = [
        {title: 'Castillo de Chambord', message: 'Su construcción comenzó en el año 1519, a pedido del rey Francisco I, a orillas del río Cosson en Francia, con influencias artísticas italianas.', url: '/historia/chambord', image: '/images/chambord.jpeg'},
        {title: 'Castillo de Chenonceau', message: 'Construído en el año 1513 por Katherine Briconnet, luego decorado por Diana de Poitiers y Catalina de Médicis.', url: '/historia/chenonceau', image: '/images/chenonceau.jpeg'},
    ];
    const lista2 = [
        {title: 'Mobilario', message: (<div>El Renacimiento Francés se divide en tres períodos:
            Francisco I (1515-1547):<br/> Se inicia el Renacimiento en Francia y luego se llega a su apogeo.<br/>
            Enrique II (1547-1559):<br/> Se dividen en dos escuelas: La del Noroeste, donde utilizaban el roble y ornamentaciones sobrias, con aun permanencia del Gótico, y del Sureste, donde utilizaban el nogal blando, por eso eran muy tallados.<br/>
            Luis XIII (1610-1643):<br/> Transición hacia el estilo Barroco.</div>), url: '/historia/mobilario', image: '/images/mobilario.jpeg'}
    ]

    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Un poco del Renacimiento Francés'
                subtitle='Entre el siglo XV y principios del siglo XVI, el Renacimiento apareció en Francia, más de medio siglo más tarde que en Italia. Una de sus características principales es que tomaba principios del arte clásico y rompía con los esquemas del arte gótico. Seguían ideales clasicistas, humanistas y antropocentristas.'
            />

            <ImgMel
                alt='cuadro'
                src='/images/historia.jpg'
            />

            <div className='w-10/12'>
                <p className='italic'>Con respecto a la Arquitectura, poseían las siguientes características:</p>

                <ul className='italic'>
                    <li>
                        <p><span className='font-black'>•</span> Tomaban cualidades del estilo Renacentista Italiano</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Construían las fachadas movidas con salientes y entradas</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Usaban torres, torrecillas y buhardas</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> Construían techos muy altos enraizados por chimeneas y cesterias</p>
                    </li>
                    <li>
                        <p><span className='font-black'>•</span> La decoración era muy importante</p>
                    </li>
                </ul>

                <p className='italic'> Un logro muy importante para el Renacimiento Francés fue la construcción de los Castillos del Valle del Loira, que en vez de usarse como fortalezas, se comenzaron a usar como viviendas.</p>

                <h2 className='font-bold text-3xl text-center mb-12'>
                    Algunas de nuestras inspiraciones
                </h2>
            </div>

            <List list={lista}/>

            <List list={lista2}/>
        </section>
    );
}