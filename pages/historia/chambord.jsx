import Title from '../../components/title.jsx';
import ImgMel from '../../components/imgMel.jsx';
import BackToHistory from '../../components/BackToHistory.jsx';

export default function Chambord() {
    return (
        <section className='w-full flex flex-col items-center justify-start pt-32'>
            <Title
                title='Castillo de Chambord'
                subtitle='Su construcción comenzó en el año 1519, a pedido del rey Francisco I, a orillas del río Cosson en Francia, con influencias artísticas italianas.'
            />
            <div className='w-10/12'>
                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-1.jpeg'
                />
                
                <p className='text-center italic'>Es uno de los pertenecientes al conjunto de los Castillos del Loira y es el más grande de ellos.</p>
            
                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-2.jpeg'
                />

                <h2 className='font-bold text-3xl text-center mb-12'>
                    Elementos arquitectónicos utilizados
                </h2>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-3.jpeg'
                />

                <h2 className='font-bold text-3xl text-center mb-12'>
                    Otros elementos arquitectónicos del Renacimiento Francés presentados en el Castillo
                </h2>

                <p className='text-center italic font-bold'>Ventanas con cornisas y sucesión de pilastras</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-4.jpeg'
                />

                <p className='text-center italic font-bold'>Capiteles con pequeñas cabezas talladas</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-5.jpeg'
                />

                <p className='text-center italic font-bold'>Terrazas</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-6.jpeg'
                />

                <p className='text-center italic font-bold'>Casetones</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-7.jpeg'
                />

                <p className='text-center italic font-bold'>Escalera de doble hélice</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-8.jpeg'
                />

                <p className='text-center italic font-bold'>Jardines</p>

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-9.jpeg'
                />

                <ImgMel
                    width='5/12'
                    altt='plano del castillo'
                    src='/images/chambord/image-10.jpeg'
                />
                
                <h2 className='font-bold text-3xl text-center mb-12'>
                    Conozcamoslo por dentro
                </h2>

                <iframe src="https://www.google.com/maps/embed?pb=!4v1664172145907!6m8!1m7!1ssSlsnpE2IptiJDfl156Q2Q!2m2!1d47.61609723437358!2d1.51715845660118!3f317.7920227050781!4f1.0802383422851562!5f0.7820865974627469" width="600" height="600" className='w-11/12 m-auto' style={{border:0 }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>

            <BackToHistory/>
        </section>
    );
}