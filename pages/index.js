import Title from '../components/title.jsx';
import ImgMel from '../components/imgMel.jsx';

export default function Home() {
  return (
    <section className='w-full flex flex-col items-center justify-start pt-32'>
      <Title
        title='¿Quiénes somos?'
        subtitle={(<div className='text-center'>
          Estudio de arquitectura y diseño con bases en el Renacimiento Francés.<br/> Reformas y proyectos con diseños únicos y de época.
        </div>)}
      />
      
      <ImgMel
          alt="interior de castillo"
          src='/images/interiores/Frame 43.png'
        />
    </section>
  )
}
