import { useRouter } from 'next/router'

export default function BackToProject() {
    const { push } = useRouter();

    return (
        <div
            onClick={() => {push('/proyectos', '/proyectos', { scroll: false })}} 
            className='flex w-full mt-8 cursor-pointer p-4'
        >
            <h4 className='text-sm ml-2 border-2 p-4 text-slate-800 border-slate-800 hover:text-red-500 hover:border-red-500'><i className="ri-arrow-left-line"/> Volver a proyectos</h4>
        </div>
    );
}
