export default function ImgMel({ src, alt , onClick, width = 'w-10/12' }) {
    return (
        <div className={`${width} overflow-hidden m-2 md:m-8`}>
            <img
                className={`w-full ease-in duration-100 hover:scale-105 ${onClick ? 'cursor-pointer' : ''}`}
                onClick={onClick}
                alt={alt}
                src={src}
            />
        </div>
    );
}