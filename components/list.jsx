import ImgMel from "./imgMel";
import { useRouter } from 'next/router';

export default function List({ list }) {
    const { push } = useRouter();

    function redirect(url) {
        push(url);
    }

    return (
        <div className="w-full flex flex-wrap justify-center flex-wrap border-t border-solid border-1 border-black-200 divide-y md:divide-y-0 md:divide-x divide-solid">
            {list.map(item => (
                <div
                    className='xs:w-full sm:w-1/2 flex flex-col items-center justify-start px-8 py-12'
                    key={item.title}
                >
                    <h2 className='text-md md:text-xl font-bold title uppercase w-full text-center'>
                        {item.title}
                    </h2>

                    <ImgMel
                        onClick={() => redirect(item.url)}
                        alt='post'
                        src={item.image}
                    />

                    <p className='w-10/12'>
                        {item.message}
                    </p>

                    <h4 onClick={() => redirect(item.url)} className='uppercase text-sm border-black border-2 px-8 py-4 hover:border-red-500 hover:text-red-500 cursor-pointer'>
                        Ver más
                    </h4>
                </div>
            ))}
        </div>
    );
}