export default function Title({ title, subtitle }) {
    return (
        <div className="w-full flex flex-col items-center justify-center w-10/12 text-center">
            <h1 className="text-5xl md:text-7xl">
                {title}
            </h1>

            <p className="py-4 italic">
                {subtitle}
            </p>
        </div>
    );
}